import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  user: User;
  chatForm: FormGroup;
  messageList: [] = [];
  @ViewChild('messages', { static: true }) messages: ElementRef;

  constructor(
    private fb: FormBuilder,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    this.chatForm = this.fb.group({
      message: [null, [Validators.required]]
    });
    this.afAuth.user.pipe(take(1)).subscribe(user => {
      this.user = user;
    });
    this.afs
      .collection('messages')
      .valueChanges()
      .subscribe((messages: []) => {
        if (messages) {
          this.messageList = messages.sort(this.compare);
          setTimeout(() => {
            this.messages.nativeElement.scrollTop = this.messages.nativeElement.scrollHeight;
          }, 1000);
        }
      });
  }
  private compare(a, b) {
    if (a.createdAt < b.createdAt) {
      return -1;
    }
    if (a.createdAt > b.createdAt) {
      return 1;
    }
    return 0;
  }
  submitForm() {
    const message = {
      message: this.chatForm.value.message,
      user: this.user.uid,
      createdAt: new Date()
    };
    this.afs
      .collection('messages')
      .add(message)
      .then(() => {
        this.messages.nativeElement.scrollTop = this.messages.nativeElement.scrollHeight;
        this.chatForm.reset();
      });
  }
}
