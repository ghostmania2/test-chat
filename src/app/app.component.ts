import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user: User;
  constructor(private afAuth: AngularFireAuth) {
    this.afAuth.user.subscribe(user => {
      this.user = user;
    });
  }
  login() {
    this.afAuth.auth
      .signInAnonymously()
      .then(res => {})
      .catch(err => {
        console.error({ error: err });
      });
  }
  logout() {
    this.afAuth.auth
      .signOut()
      .then(res => {})
      .catch(err => {
        console.error({ error: err });
      });
  }
}
